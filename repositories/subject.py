from sqlalchemy.orm import Session

from domain.models.subject import Disciplina
from domain.models.student import Aluno
from domain.models.enrollment import Matricula

def get_subjects(db: Session):
    return db.query(Disciplina).all()

def get_subject_by_id(db: Session, subject_id:int):
    return db.query(Disciplina).filter(Disciplina.subject_id==subject_id).first()

def get_subject_by_code(db:Session,code:str):
    return db.query(Disciplina).filter(Disciplina.code == code).first()

def create_subject(db:Session, subject:Disciplina):
    db.add(subject)
    db.commit()
    db.refresh(subject)
    return subject

def assign_professor_to_subject(db: Session, subject_id:int,professor_id:int,subject: Disciplina):
    db.query(Disciplina).filter(Disciplina.subject_id == subject_id).update({"professor_id": professor_id})
    db.commit()
    db.refresh(subject)
    return subject

def get_students_of_subject(db:Session,subject_id:int):
    return db.query(Aluno).join(Matricula).filter(Matricula.subject_id == subject_id).all()
