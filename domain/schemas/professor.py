from pydantic import Field
from typing import List

from domain.schemas.generic import GenericModel,BaseModel

class ProfessorBase(GenericModel):
    CPF: str = Field(title='CPF of professor', regex='[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}')
    professor_name: str = Field(title='Name of professor')
    professor_academic_degree: str = Field(title='Academic degree of professor')
    class Config:
        orm_mode = True
        schema_extra={
            'example':{
                'professor_name':'Wagner Pimentel',
                'CPF':'105.924.865-60',
                'professor_academic_degree':'Doutorado'
            }
        }

class Professor(ProfessorBase):
    professor_id: int = Field(title='ID of professor')
    class Config:
        orm_mode = True
        schema_extra={
            'example':{
                'professor_name':'Wagner Pimentel',
                'CPF':'105.924.865-60',
                'professor_academic_degree':'Doutorado',
                'professor_id':'4'
            }
        }

class ProfessorList(BaseModel):
    __root__: List[Professor]
    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            'title': 'List of professors'
        }