from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from domain.schemas.student import Aluno,AlunoBase,AlunoList,Curso
from config.database import get_session
from services import student as student_service

router =APIRouter()
student_tag = 'Student'

@router.get(
    '/student',
    summary = 'Fetch all students data',
    tags=[student_tag],
    response_model=AlunoList
)
def read_students(db:Session = Depends(get_session)):
    return student_service.get_students(db)

@router.get(
    '/student/{student_id}',
    summary = 'Fetch one student data',
    tags=[student_tag],
    response_model=Aluno
)
def read_student(student_id:int,db:Session = Depends(get_session)):
    return student_service.get_student_by_id(db,student_id)

@router.post(
    '/student',
    summary='Create new student',
    tags=[student_tag],
    response_model=Aluno
)
def create_student(student:AlunoBase,db: Session = Depends(get_session)):
    return student_service.create_student(db,student)

@router.put(
    '/student/{student_id}/course/{course_id}',
    summary='Set course to student',
    tags=[student_tag],
    response_model=Aluno
)
def assign_course_to_student(student_id:int,course_id:int,db:Session = Depends(get_session)):
    return student_service.assign_course_to_student(db,student_id,course_id)

@router.post(
    '/student/{student_id}/subject/{subject_id}',
    summary = 'Enroll student in a class',
    tags = [student_tag],
    response_model=Aluno
)
def enroll_student(student_id:int,subject_id:int,db:Session=Depends(get_session)):
    return student_service.enroll_student(db,student_id,subject_id)

@router.delete(
    '/student/{student_id}/subject/{subject_id}',
    summary='Unenroll student in a subject',
    tags=[student_tag],
    response_model=Aluno
)
def unenroll_student_in_class(student_id:int,subject_id:int,db:Session = Depends(get_session)):
    return student_service.remove_student_from_subject(db,student_id,subject_id)
