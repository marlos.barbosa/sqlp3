from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from domain.schemas.subject import Disciplina,DisciplinaList,DisciplinaBase
from domain.schemas.student import AlunoList
from config.database import get_session
from services import subject as subject_service

router =APIRouter()
subject_tag = 'Subject'

@router.get(
    '/subject',
    summary='Fetch all subjects data',
    tags = [subject_tag],
    response_model = DisciplinaList
)
def read_subjects(db:Session = Depends(get_session)):
    return subject_service.get_subjects(db)

@router.get(
    '/subject/{subject_id}',
    summary='Fetch one subject data',
    tags = [subject_tag],
    response_model = Disciplina
)
def read_subject(subject_id:int,db:Session = Depends(get_session)):
    return subject_service.get_subject_by_id(db,subject_id)

@router.post(
    '/subject',
    summary='Create new subject',
    tags = [subject_tag],
    response_model = Disciplina
)
def create_new_subject(subject: DisciplinaBase,db:Session = Depends(get_session)):
    return subject_service.create_subject(db,subject)

@router.put(
    '/subject/{subject_id}/professor/{professor_id}',
    summary='Assign a subject to a professor',
    tags = [subject_tag],
    response_model = Disciplina
)
def assign_subject(subject_id: int,professor_id:int,db:Session = Depends(get_session)):
    return subject_service.assign_professor_to_subject(db,subject_id,professor_id)

@router.get(
    '/subject/{subject_id}/student',
    summary='Fetch data of enrolled students',
    tags = [subject_tag],
    response_model = AlunoList
)
def read_subject(subject_id:int,db:Session = Depends(get_session)):
    return subject_service.get_students_of_subject(db,subject_id)