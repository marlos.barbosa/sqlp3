from pydantic import Field
from typing import List

from domain.schemas.generic import GenericModel, BaseModel
from domain.schemas.course import Curso
from domain.schemas.subject import DisciplinaList

class AlunoBase(GenericModel):
    CPF: str = Field(title='CPF of student', regex='[0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[\-]?[0-9]{2}')
    student_name: str = Field(title='Name of student')
    birthday_date: str | None = Field(title='Birthday of student')
    RG: str = Field(title='RG of student')
    dispatching_agency: str = Field(title='Dispatching agency of student RG')
    class Config:
        orm_mode=True
        schema_extra = {
            'example': {
                'CPF': '123.369.521-74',
                'student_name': 'Marlos Ferreira Barbosa',
                'birthday_date':'26/02/2001',
                'RG':  '3852992-1',
                'dispatching_agency':'SEDS/AL'
            }
        }



class Aluno(AlunoBase):
    student_id: int = Field(title='ID of student')
    course: Curso | None = Field(title='Course of student')
    classes: DisciplinaList | None = Field(title='Classes of student')
    class Config:
        orm_mode=True
        schema_extra = {
            'example': {
                'CPF': '123.369.521-74',
                'student_name': 'Marlos Ferreira Barbosa',
                'bithday_date':'26/02/2001',
                'RG':  '3852992-1',
                'dispatching_agency':'SEDS/AL',
                'student_id': '7',
                'course': {
                    'course_name': 'Engenharia Química',
                    'creation_date': '26/01/2000',
                    'building_name': 'CTEC',
                    'course_id': '5',
                    'coordinator':{
                        'professor_name':'Wagner Pimentel',
                        'CPF':'105.924.865-60',
                        'professor_academic_degree':'Doutorado',
                        'professor_id':'4'
                    }
                },
                'classes': [
                    {
                'subject_name':'Termodinâmica 2',
                'code':'EQUI-0103',
                'description':'The only interesting subject in the course so far.',
                'subject_id':'4',
                'professor':{
                    'professor_name':'Wagner Pimentel',
                    'CPF':'105.924.865-60',
                    'professor_academic_degree':'Doutorado',
                    'professor_id':'4'
                        }
                    },
                    {
                'subject_name':'Termodinâmica 1',
                'code':'EQUI-0102',
                'description':'The worst subject in the course so far.',
                'subject_id':'5',
                'professor':{
                    'professor_name':'William',
                    'CPF':'105.924.865-80',
                    'professor_academic_degree':'Doutorado',
                    'professor_id':'5'
                    }
                    }
                ]
            }
        }



class AlunoList(BaseModel):
    __root__: List[Aluno]

    class Config:
        orm_mode=True
        arbitrary_types_allowed = True
        schema_extra = {
            'title': 'List of students'
        }
