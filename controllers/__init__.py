from . import student
from . import professor
from . import subject
from . import course

routes = [
    student.router,
    professor.router,
    subject.router,
    course.router,
]
