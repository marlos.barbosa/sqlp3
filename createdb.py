from config.database import engine
from domain.models.generic import GenericBase

from domain.models import (
    course,
    enrollment,
    professor,
    subject,
    student,
)

GenericBase.metadata.create_all(bind=engine)
