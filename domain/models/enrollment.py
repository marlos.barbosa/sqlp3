from sqlalchemy import Column, ForeignKey, Integer,UniqueConstraint
from sqlalchemy.orm import relationship

from domain.models.generic import GenericBase

class Matricula(GenericBase):
    __tablename__ = "ALUNOS_DISCIPLINAS"

    enrollment_id=Column(Integer, primary_key=True, autoincrement=True)

    subject_id = Column(Integer, ForeignKey("DISCIPLINAS.subject_id"))
    student_id = Column(Integer, ForeignKey("ALUNOS.student_id"))

    __table_args__ = (
        UniqueConstraint('subject_id','student_id'),
    )