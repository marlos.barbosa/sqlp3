from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from config.database import get_session
from domain.schemas.professor import ProfessorList,Professor,ProfessorBase
from services import professor as professor_service

router =APIRouter()
professor_tag = 'Professor'

@router.get(
    '/professor',
    summary = 'Fetch all professors data',
    tags = [professor_tag],
    response_model=ProfessorList
)
def read_professors(db:Session = Depends(get_session)):
    return professor_service.get_professors(db)

@router.get(
    '/professor/{professor_id}',
    summary = 'Fetch one professor data',
    tags = [professor_tag],
    response_model=Professor
)
def read_professor(professor_id:int,db:Session = Depends(get_session)):
    return professor_service.get_professor_by_id(db,professor_id)

@router.post(
    '/professor/{professor_id}',
    summary = 'Create professor',
    tags = [professor_tag],
    response_model=Professor
)
def create_professor(professor: ProfessorBase, db:Session = Depends(get_session)):
    return professor_service.create_professor(db,professor)