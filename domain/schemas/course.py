from pydantic import Field
from typing import List

from domain.schemas.generic import GenericModel,BaseModel
from domain.schemas.professor import Professor

class CursoBase(GenericModel):
    course_name: str = Field(title='Name of course')
    creation_date: str = Field(title='Creation date of course')
    building_name: str= Field(title='Building name of course')
    class Config:
        orm_mode = True
        schema_extra={
            'example':{
                'course_name':'Engenharia Química',
                'creation_date':'26/01/2000',
                'building_name':'CTEC'
            }
        }


class Curso(CursoBase):
    course_id: int = Field(title='ID of course')
    coordinator: Professor | None = Field(title='Coordinator of course')
    class Config:
        orm_mode = True
        schema_extra={
            'example':{
                'course_name':'Termodinâmica 2',
                'creation_date':'EQUI-0103',
                'building_name':'The only interesting subject in the course so far.',
                'course_id':'4',
                'coordinator':{
                    'professor_name':'Wagner Pimentel',
                    'CPF':'105.924.865-60',
                    'professor_academic_degree':'Doutorado',
                    'professor_id':'4'
                }
            }
        }

class CursoList(BaseModel):
    __root__: List[Curso]
    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            'title': 'List of courses'
        }