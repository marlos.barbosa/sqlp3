from fastapi import HTTPException
from sqlalchemy.orm import Session

from services.course import get_course_by_id
from services.subject import get_subject_by_id
from domain.models.student import Aluno
from domain.models.enrollment import Matricula
from domain.schemas.student import AlunoBase
from repositories import student as student_repository

def get_students(db: Session):
    return student_repository.get_students(db)

def get_student_by_id(db:Session, student_id:int):
    db_student = student_repository.get_student_by_id(db,student_id)
    if db_student is None:
        raise HTTPException(status_code=400, detail='There is no student with this ID')
    return db_student

def get_student_by_CPF(db:Session,CPF:str):
    return student_repository.get_student_by_CPF(db,CPF)

def create_student(db:Session,student:AlunoBase):
    db_student = get_student_by_CPF(db,student.CPF)
    if db_student:
        raise HTTPException(status_code=400,detail='This student is already registered!')
    db_student = Aluno(**student.dict())
    return student_repository.create_student(db,db_student)

def assign_course_to_student(db:Session, student_id: int, course_id: int):
    student = get_student_by_id(db,student_id)
    course = get_course_by_id(db,course_id)
    if student is None or course is None:
        raise HTTPException(status_code=400, detail='Provide different ID')
    return student_repository.assign_course_to_student(db,student_id,course_id,student)

def enroll_student(db:Session,student_id:int,subject_id:int):
    student = get_student_by_id(db,student_id)
    subject = get_subject_by_id(db,subject_id)
    if student is None or subject is None:
        raise HTTPException(status_code=400, detail='Provide different ID')
    enrollment = check_enrollment(db,student_id,subject_id)
    if len(enrollment)!=0:
        raise HTTPException(status_code=400, detail='The student is already enrolled!')
    matricula = Matricula(student_id=student_id,subject_id=subject_id)
    return student_repository.enroll_student(db,student,matricula)


def remove_student_from_subject(db:Session,student_id:int,subject_id:int):
    student = get_student_by_id(db,student_id)
    subject = get_subject_by_id(db,subject_id)
    if student is None or subject is None:
        raise HTTPException(status_code=400, detail='Provide different ID')
    check_enrollmentl=check_enrollment(db,student_id,subject_id)
    if len(check_enrollmentl)==0:
        raise HTTPException(status_code=400, detail='The student is not enrolled in this subject')
    return student_repository.remove_student_from_subject(db,student_id,subject_id,student)

def check_enrollment(db:Session,student_id:int,subject_id:int):
    return student_repository.check_enrollment(db,student_id,subject_id)