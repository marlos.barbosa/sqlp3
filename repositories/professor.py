from sqlalchemy.orm import Session

from domain.models.professor import Professor

def get_professors(db:Session):
    return db.query(Professor).all()

def get_professor_by_id(db:Session,professor_id=int):
    return db.query(Professor).filter(Professor.professor_id==professor_id).first()

def get_professor_by_CPF(db:Session,CPF=int):
    return db.query(Professor).filter(Professor.CPF==CPF).first()

def create_professor(db:Session, professor: Professor):
    db.add(professor)
    db.commit()
    db.refresh(professor)
    return professor