from sqlalchemy.orm import Session

from domain.models.course import Curso
from domain.models.student import Aluno

def get_courses(db:Session):
    return db.query(Curso).all()

def get_course_by_id(db:Session, course_id:int):
    return db.query(Curso).filter(Curso.course_id==course_id).first()

def get_course_by_name(db:Session,course_name:str):
    return db.query(Curso).filter(Curso.course_name==course_name).first()

def create_course(db:Session,course:Curso):
    db.add(course)
    db.commit()
    db.refresh(course)
    return course

def assign_coordinator_to_course(db:Session,course_id:int,coordinator_id:int, course: Curso):
    db.query(Curso).filter(Curso.course_id == course_id).update({"coordinator_id": coordinator_id})
    db.commit()
    db.refresh(course)
    return course

def get_students_of_course(db:Session,course_id:int):
    return db.query(Aluno).filter(Aluno.course_id==course_id).all()