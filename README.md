# FastAPI using SQL (relational) database with a layered architecture

## Setup
Install Python
- Install [Python](https://www.python.org/downloads) (version 3.10).

Main frameworks and packages: 
- [FastAPI](https://fastapi.tiangolo.com)
- [SQLAlchemy](https://https://www.sqlalchemy.org)

## Set and activate virtual environment
In project folder, execute the following commands:

```bash
pip install pipenv
export PIPENV_VENV_IN_PROJECT="enabled"
mkdir .venv
pipenv shell
source .venv/Scripts/activate
```

## Install dependencies on virtual env
In project folder, execute the following command:

```bash
pipenv install --dev
```

## Set environment variables
In project folder, create a *.env* file from *.env.example*
```bash
cp .env.example .env
```
Set proper values for the environment variables on *.env*

## Run
```bash
python main.py
```
