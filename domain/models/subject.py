from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from domain.models.generic import GenericBase

class Disciplina(GenericBase):
    __tablename__ = "DISCIPLINAS"

    subject_id = Column(Integer, primary_key=True,autoincrement=True)
    subject_name = Column(String)
    code = Column(String)
    description = Column(String)
    professor_id = Column(Integer, ForeignKey("PROFESSORES.professor_id"))

    professor = relationship("Professor", back_populates="classes")
    student = relationship("Aluno", secondary='ALUNOS_DISCIPLINAS', back_populates="classes")