from sqlalchemy.orm import Session

from domain.models.student import Aluno
from domain.models.enrollment import Matricula

def get_students(db: Session):
    return db.query(Aluno).all()

def get_student_by_id(db:Session, student_id:int):
    return db.query(Aluno).filter(Aluno.student_id==student_id).first()

def get_student_by_CPF(db:Session,CPF:str):
    return db.query(Aluno).filter(Aluno.CPF==CPF).first()

def create_student(db:Session,student:Aluno):
    db.add(student)
    db.commit()
    db.refresh(student)
    return student

def assign_course_to_student(db:Session, student_id: int, course_id: int,student:Aluno):
    db.query(Aluno).filter(Aluno.student_id == student_id).update({"course_id": course_id})
    db.commit()
    db.refresh(student)
    return student

def enroll_student(db:Session,student:Aluno,matricula:Matricula):
    db.add(matricula)
    db.commit()
    db.refresh(student)
    return student


def remove_student_from_subject(db:Session,student_id:int,subject_id:int,student:Aluno):
    db.query(Matricula).filter(Matricula.student_id==student_id).filter(Matricula.subject_id==subject_id).delete()
    db.commit()
    db.refresh(student)
    return student

def check_enrollment(db:Session,student_id:int,subject_id:int):
    return db.query(Matricula).filter(Matricula.student_id==student_id).filter(Matricula.subject_id==subject_id).all()