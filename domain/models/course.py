from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from domain.models.generic import GenericBase

class Curso(GenericBase):
    __tablename__ = "CURSOS"

    course_id = Column(Integer, primary_key=True,autoincrement=True)
    course_name = Column(String)
    creation_date = Column(String)
    building_name = Column(String)
    coordinator_id = Column(Integer, ForeignKey("PROFESSORES.professor_id"))

    coordinator = relationship("Professor", back_populates="course")
    students=relationship("Aluno", back_populates="course")