from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from domain.models.generic import GenericBase

class Professor(GenericBase):
    __tablename__ = "PROFESSORES"

    professor_id = Column(Integer, primary_key=True)
    CPF = Column(String, unique = True)
    professor_name = Column(String)
    professor_academic_degree = Column(String)

    course = relationship("Curso", back_populates = "coordinator")
    classes = relationship("Disciplina", back_populates="professor")