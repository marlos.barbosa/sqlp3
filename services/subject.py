from fastapi import HTTPException
from sqlalchemy.orm import Session

from services.professor import get_professor_by_id
from domain.models.subject import Disciplina
from domain.schemas.subject import DisciplinaBase
from repositories import subject as subject_repository

def get_subjects(db: Session):
    return subject_repository.get_subjects(db)

def get_subject_by_id(db: Session, subject_id:int):
    db_subject = db.query(Disciplina).filter(Disciplina.subject_id==subject_id).first()
    if db_subject is None:
        raise HTTPException(status_code=400, detail='Subject does not exist yet')
    return db_subject

def get_subject_by_code(db:Session,code:str):
    return subject_repository.get_subject_by_code(db,code)



def create_subject(db:Session, subject:DisciplinaBase):
    db_subject = get_subject_by_code(db,subject.code)
    if db_subject:
        raise HTTPException(status_code=400,detail='The subject already exists')
    db_subject = Disciplina(**subject.dict())
    return subject_repository.create_subject(db,db_subject)


def assign_professor_to_subject(db: Session, subject_id:int,professor_id:int):
    subject = get_subject_by_id(db,subject_id)
    professor = get_professor_by_id(db,professor_id)
    if subject is None or professor is None:
        raise HTTPException(status_code=400, detail='Provide different ID')
    return subject_repository.assign_professor_to_subject(db,subject_id,professor_id,subject)

def get_students_of_subject(db:Session,subject_id:int):
    db_subject = get_subject_by_id(db,subject_id)
    if db_subject is None:
        raise HTTPException(status_code=400, detail='Subject does not exist')
    return subject_repository.get_students_of_subject(db,subject_id)
