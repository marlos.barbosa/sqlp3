from fastapi import HTTPException
from sqlalchemy.orm import Session

from domain.models.professor import Professor
from domain.schemas.professor import ProfessorBase
from repositories import professor as professor_repository

def get_professors(db:Session):
    return professor_repository.get_professors(db)

def get_professor_by_id(db:Session,professor_id=int):
    db_professor = professor_repository.get_professor_by_id(db,professor_id)
    if db_professor is None:
        raise HTTPException(status_code=400, detail='There is no professor with this ID')
    return db_professor

def get_professor_by_CPF(db:Session,CPF=int):
    return professor_repository.get_professor_by_CPF(db,CPF)

def create_professor(db:Session, professor: ProfessorBase):
    db_professor = get_professor_by_CPF(db,professor.CPF)
    if db_professor:
        raise HTTPException(status_code=400, detail='There is already a professor with this CPF')
    db_professor = Professor(**professor.dict())
    return professor_repository.create_professor(db,db_professor)