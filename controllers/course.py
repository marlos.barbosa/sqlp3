from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from config.database import get_session
from domain.schemas.course import Curso,CursoList,CursoBase
from domain.schemas.student import AlunoList
from services import course as course_service

router =APIRouter()

course_tag= 'Course'

@router.get(
    '/course',
    summary = 'Fetch all courses data',
    tags=[course_tag],
    response_model=CursoList
)
def read_courses(db:Session = Depends(get_session)):
    return course_service.get_courses(db)

@router.get(
    '/course/{course_id}',
    summary = 'Fetch one course data',
    tags=[course_tag],
    response_model=Curso
)
def read_course(course_id:int,db:Session = Depends(get_session)):
    return course_service.get_course_by_id(db,course_id)

@router.post(
    '/course',
    summary='Create new course',
    tags=[course_tag],
    response_model=Curso
)
def create_course(course:CursoBase,db: Session = Depends(get_session)):
    return course_service.create_course(db,course)

@router.put(
    '/course/{course_id}/coordinator/{professor_id}',
    summary='Set coordinator to course',
    tags=[course_tag],
    response_model=Curso
)
def assing_coordinator_to_course(course_id:int,professor_id:int,db:Session = Depends(get_session)):
    return course_service.assign_coordinator_to_course(db,course_id,professor_id)

@router.get(
    '/course/{course_id}/student',
    summary='Fetch students from a specific course',
    tags=[course_tag],
    response_model=AlunoList
)
def read_students_from_course(course_id: int, db: Session = Depends(get_session)):
    return course_service.get_students_of_course(db,course_id)