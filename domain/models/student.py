from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from domain.models.generic import GenericBase

class Aluno(GenericBase):
    __tablename__ = "ALUNOS"

    student_id = Column(Integer, primary_key=True,autoincrement=True)
    CPF = Column(String, unique = True)
    student_name = Column(String)
    birthday_date = Column(String)
    RG = Column(String)
    dispatching_agency = Column(String)
    course_id = Column(Integer, ForeignKey("CURSOS.course_id"))


    course = relationship("Curso",back_populates="students")
    classes = relationship("Disciplina", secondary='ALUNOS_DISCIPLINAS', back_populates = "student")