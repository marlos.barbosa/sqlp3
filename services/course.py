from fastapi import HTTPException
from sqlalchemy.orm import Session

from services.professor import get_professor_by_id
from domain.models.course import Curso
from domain.schemas.course import CursoBase
from repositories import course as course_repository

def get_courses(db:Session):
    return course_repository.get_courses(db)

def get_course_by_id(db:Session, course_id:int):
    db_course = course_repository.get_course_by_id(db,course_id)
    if db_course is None:
        raise HTTPException(status_code=400, detail='There is no course with this ID')
    return db_course

def get_course_by_name(db:Session,course_name:str):
    return course_repository.get_course_by_name(db,course_name)

def create_course(db:Session,course:CursoBase):
    db_course = get_course_by_name(db,course.course_name)
    if db_course:
        raise HTTPException(status_code=400,detail='This course is already registered!')
    db_course = Curso(**course.dict())
    return course_repository.create_course(db,db_course)

def assign_coordinator_to_course(db:Session, course_id: int, coordinator_id: int):
    course = get_course_by_id(db,course_id)
    professor = get_professor_by_id(db,coordinator_id)
    if professor is None or course is None:
        raise HTTPException(status_code=400, detail='Provide different ID')
    return course_repository.assign_coordinator_to_course(db,course_id,coordinator_id,course)

def get_students_of_course(db:Session,course_id:int):
    db_course = get_course_by_id(db,course_id)
    if db_course is None:
        raise HTTPException(status_code = 400, detail='There is no course with this ID')
    return course_repository.get_students_of_course(db,course_id)