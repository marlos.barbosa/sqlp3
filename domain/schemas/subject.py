from pydantic import Field
from typing import List

from domain.schemas.generic import GenericModel, BaseModel
from domain.schemas.professor import Professor

class DisciplinaBase(GenericModel):
    subject_name: str = Field(title='Name of subject')
    code: str = Field(title='Code of subject')
    description: str = Field(title='Description of subject')
    class Config:
        orm_mode = True
        schema_extra={
            'example':{
                'subject_name':'Termodinâmica 2',
                'code':'EQUI-0103',
                'description':'The only interesting subject in the course so far.'
            }
        }

class Disciplina(DisciplinaBase):
    subject_id: int = Field(title='ID of subject')
    professor: Professor | None = Field(title = 'Professor of the subject')
    class Config:
        orm_mode = True
        schema_extra={
            'example':{
                'subject_name':'Termodinâmica 2',
                'code':'EQUI-0103',
                'description':'The only interesting subject in the course so far.',
                'subject_id':'4',
                'professor':{
                    'professor_name':'Wagner Pimentel',
                    'CPF':'105.924.865-60',
                    'professor_academic_degree':'Doutorado',
                    'professor_id':'4'
                }
            }
        }

class DisciplinaList(BaseModel):
    __root__: List[Disciplina]
    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            'title': 'List of subjects'
        }